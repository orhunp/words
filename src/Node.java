class Node<T> {
    private final T data;
    private int frequency;
    private Node<T> next;

    Node(T data) {
        this.data = data;
        this.next = null;
        this.frequency = 1;
    }

    T getData() {
        return data;
    }

    Node<T> getNext() {
        return next;
    }

    void setNext(Node<T> next) {
        this.next = next;
    }

    int getFrequency() {
        return frequency;
    }

    void freq() {
        this.frequency++;
    }
}
