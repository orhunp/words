import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        LinkedList<String> words = new LinkedList<>();
        ArrayList<String> lines = new SanicRead().readLikeSanic("input.txt");
        Pattern pattern = Pattern.compile("(?U)\\b[a-zA-Z]+\\b");
        for (int i = 0; i < lines.size(); i++) {
            Matcher matcher = pattern.matcher(lines.get(i));
            while (matcher.find()) {
                String word = matcher.group(0).toLowerCase();
                System.out.printf("PROGRESS: %s/%s [%s] {%s}\n", i + 1, lines.size(),
                        words.add(word) ? "NEW" : "EXISTS", word);
            }
        }
        List<Node<String>> sortedWords = words.asList();
        sortedWords.sort((Node n1, Node n2) -> n1.getData().toString()
                .compareToIgnoreCase(n2.getData()
                .toString()));
        try (FileWriter writer = new FileWriter("output.txt")) {
            for (Node<String> node : sortedWords) {
                String result = String.format(Locale.US, "%-20s => %d\n", node.getData(), node.getFrequency());
                writer.write(result);
                System.out.print(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
