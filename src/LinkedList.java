import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LinkedList<T> implements ILinkedList<T> {

    private Node<T> head;

    LinkedList() {
        this.head = null;
    }

    @Override
    public int size() {
        int size = 0;
        Node current = head;
        while (current != null) {
            size++;
            current = current.getNext();
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public boolean add(T entry) {
        if (this.check(entry)) {
            return false;
        }
        Node<T> node = new Node<>(entry);
        if (head == null) {
            head = node;
        } else {
            Node<T> last = head;
            while (last.getNext() != null) {
                last = last.getNext();
            }
            last.setNext(node);
            /*-----------------------------------------------------------------
            |  Following code block adds an item to the linked list
            |  while caring about the alphabetical order.
            |  It's commented out because IT WORKS SLOW.
            |  I used a List for ordering the items afterwards, works much faster.
            | - orhun
            *------------------------------------------------------------------ */
            /*Node<T> last = head;
            while (last.getNext() != null && entry.toString().compareTo(last.getData().toString()) >= 0) {
                last = last.getNext();
            }
            if (last.equals(head) && entry.toString().compareTo(last.getData().toString()) < 0) {
                node.setNext(head);
                head = node;
            } else {
                node.setNext(last.getNext());
                last.setNext(node);
            }*/
        }
        return true;
    }

    @Override
    public boolean remove(T entry) {
        Node<T> current = head;
        Node<T> previous = current;
        while (current != null) {
            if (entry.equals(current.getData())) {
                if (current.equals(head)) {
                    head = head.getNext();
                } else {
                    previous.setNext(current.getNext());
                }
                return true;
            }
            previous = current;
            current = current.getNext();
        }
        return false;
    }

    @Override
    public void clear() {
        this.head = null;
    }

    @Override
    public boolean contains(T entry) {
        Node current = head;
        while (current != null) {
            if (entry.equals(current.getData())) {
                return true;
            }
            current = current.getNext();
        }
        return false;
    }

    private boolean check(T entry) {
        Node current = head;
        while (current != null) {
            if (entry.equals(current.getData())) {
                current.freq();
                return true;
            }
            current = current.getNext();
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Node<T>> asList() {
        List<Node<T>> list = new ArrayList<>();
        Node<T> current = head;
        while (current != null) {
            list.add(current);
            current = current.getNext();
        }
        return list;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Node<T> current = head;
        while (current != null) {
            builder.append(String.format(Locale.US, "%-20s => %d\n",
                    current.getData().toString(), current.getFrequency()));
            current = current.getNext();
        }
        return builder.toString();
    }
}
