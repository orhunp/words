import java.util.List;

public interface ILinkedList<T> {
    int size();
    boolean isEmpty();
    boolean add(T entry);
    boolean remove(T entry);
    void clear();
    boolean contains(T entry);
    List<Node<T>> asList();
    String toString();
}
