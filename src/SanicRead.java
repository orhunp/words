import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/* This thing reads the file SO FAST that it might be a lost cousin of sonic the hedgehog */
class SanicRead {
    ArrayList<String> readLikeSanic(String path) {
        ArrayList<String> list = new ArrayList<>();
        try {
            final BufferedReader in = new BufferedReader(
                    new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8));
            String line;
            while ((line = in.readLine()) != null) {
                list.add(line);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
